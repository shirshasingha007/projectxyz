import React from "react";
import { Route } from "react-router-dom";
import "./App.css";

import Navbar from "./components/navbar/navbar";
import LeftSideBar from "./components/leftsidebar/leftsidebar";
import ResearchNav from "./components/researchpanel/researchpanel";
import TopLeft1 from "./components/graphs/topleft1/topleft1";
import TopCenter1 from "./components/graphs/topcenter1/topcenter1"
import TopRight1 from "./components/graphs/topright1/topright1"
import CenterMain from "./components/graphs/pagecentermain/pagecentermain";
import BottomOneFirst from "./components/graphs/bottomone1st/bottomone1st";
import BottomOneSecond from "./components/graphs/bottomone2nd/bottomone2nd";
import BottomOneThird from "./components/graphs/bottomone3rd/bottomone3rd";
import BottomTwoFirst from "./components/graphs/bottomtwo1st/bottomtwo1st";
import BottomTwoSecond from "./components/graphs/bottomtwo2nd/bottomtwo2nd";
import BottomTwoThird from "./components/graphs/bottomtwo3rd/bottomtwo3rd";


import { FileViewerContainer } from "./components/FileViewerContainer";


function App() {
  return (
    <div className="App">
      <Route exact path="/">
      <Navbar />
      <LeftSideBar />
      <ResearchNav />
      <TopLeft1 />
      <TopCenter1 />
      <TopRight1 />
      <CenterMain />
      <BottomOneFirst />
      <BottomOneSecond />
      <BottomOneThird /> 
      <BottomTwoFirst />
      <BottomTwoSecond />
      <BottomTwoThird />
      </Route>
      <Route path="/codeviewer" exact component={FileViewerContainer}/>

    </div>
  );
}

export default App;