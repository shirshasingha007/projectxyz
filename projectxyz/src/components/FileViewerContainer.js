import React from 'react';
import { FileTree } from './FileTree';
import files from '../data/files.json'

export const FileViewerContainer = () => {
    return (
        <div className="FileViewerContainer">
            {files.map((item,i) => (
                <FileTree key={i} data={item}/>
            ))}
        </div>
    )
}

