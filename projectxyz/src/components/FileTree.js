import React from 'react';
import { FilePreview } from './FilePreview';

export const FileTree = ({data}) => {
    const [preview,setPreview] = React.useState()
    const lorem = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt nibh at diam auctor suscipit. Vivamus vel ullamcorper risus. Sed porta mauris id ante cursus porta. Curabitur sit amet dolor quam. Nulla facilisi. Vivamus vitae nunc quis ligula facilisis interdum in ut nunc. Nunc auctor arcu vitae mattis pretium. Donec feugiat, lorem eget mollis convallis, nulla elit tincidunt libero, a feugiat arcu purus ac nunc. Maecenas consectetur volutpat dolor a pellentesque. In vulputate, lacus sit amet tempor fermentum, magna felis ultrices turpis, nec tincidunt enim ex ac lacus.

    Maecenas accumsan fermentum dui feugiat fringilla. Proin ipsum diam, aliquam at tincidunt in, scelerisque quis elit. Etiam at elit egestas elit congue varius in eget nulla. Nullam eros quam, pulvinar ut arcu et, ultricies bibendum nulla. Duis viverra imperdiet elit, ullamcorper blandit nibh. Mauris id tortor mauris. Cras imperdiet et turpis quis vestibulum. Aenean non metus dolor. Nunc porttitor maximus quam, eget facilisis massa porttitor vitae. Duis condimentum nisi nulla, a interdum libero tincidunt vitae. Nullam suscipit ex nisl, vel venenatis velit pellentesque viverra. Sed a augue eros. Ut tincidunt id libero eget pharetra. Suspendisse sagittis bibendum dui, nec vulputate velit. Aliquam aliquet pretium tempus.`

    const handleClick = (e) => {
        setPreview(`previewing data from ${e.target.innerText}`)
    }

    return(
        <div className="FileTree">
            <div className="FileTreeView">
            <p>{data.name}</p>
             {data.files.map((file,i) => (
                 <p key={i} onClick={handleClick}>{file}</p>
             ))}
             {data.dirs.map((dirs,i) => (
                 <p key={i} onClick={handleClick}>{dirs} </p>
             ))}
            </div>
            <div className="FilePreview">
            <FilePreview data={preview} lorem={lorem}/>
            </div>
        </div>
    );
}