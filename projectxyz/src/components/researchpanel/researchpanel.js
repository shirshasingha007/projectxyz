import React, { Component } from "react";
import "./researchpanel.css";

class ResearchNav extends Component {
  render() {
    return (
      <div className="researchpanel">
        <h1 className="title">Research & Experiments</h1>
        <ColoredLine color="black" />
      </div>
    );
  }
}

const ColoredLine = ({ color }) => (
  <hr
    style={{
      color: color,
      backgroundColor: color,
      height: 5,
    }}
  />
);

export default ResearchNav;
