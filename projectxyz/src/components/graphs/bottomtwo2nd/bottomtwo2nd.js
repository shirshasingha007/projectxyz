import React, { Component } from "react";
import { Line } from "react-chartjs-2";
import "./bottomtwo2nd.css";

const state = {
  labels: [
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
  ],
  datasets: [
    {
      // label: "Rainfall",
      backgroundColor: "rgba(75,192,192,1)",
      borderColor: "rgba(0,0,0,1)",
      borderWidth: 2,
      data: [65, 59, 80, 81, 59, 56, 69,],
      fill: false,
    },

    {
      // label: "Rainfall",
      backgroundColor: "rgba(75,180,190,1)",
      borderColor: "rgba(0,0,0,1)",
      borderWidth: 2,
      data: [69, 57, 80, 81, 59, 78, 69,],
      fill: false,
    },

  ],
};

class BottomTwoSecond extends Component {
  render() {
    return (
      <div className="SecondTwoBottomCard">
        <div className="SecondTwoBottomGraph">
        <Line
          data={state}
          options={{
            title: {
              // display: true,
              //   text: "Average Rainfall per month",
              fontSize: 20,
            },
            legend: {
              // display: true,
              // position: "chartArea",
            },
          }}
        />
        {/* </div>
        <h6>Hello test world</h6> */}
        </div>
      </div>
    );
  }
}

export default BottomTwoSecond;

