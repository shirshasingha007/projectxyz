import React, { Component } from "react";
import { Line } from "react-chartjs-2";
import "./topleft1.css";

const state = {
  labels: [
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
  ],
  datasets: [
    {
      // label: "Rainfall",
      backgroundColor: "rgba(75,192,192,1)",
      borderColor: "rgba(0,0,0,1)",
      borderWidth: 2,
      data: [65, 59, 40, 81, 59, 56, 69, 75,],
      fill: false,
    },

    {
      // label: "Rainfall",
      backgroundColor: "rgba(75,180,190,1)",
      borderColor: "rgba(0,0,0,1)",
      borderWidth: 2,
      data: [69, 57, 80, 100, 59, 78, 69, 100,],
      fill: false,
    },
  ],
};

class TopLeft1 extends Component {
  render() {
    return (
      <div className="topleftcard">
        <div className="topleftgraph">
          <Line
            data={state}
            options={{
              title: {
                // display: true,
                //   text: "Average Rainfall per month",
                fontSize: 20,
              },
              legend: {
                // display: true,
                // position: "chartArea",
              },
            }}
          />
          {/* </div>
        <h6>Hello test world</h6> */}
        </div>
      </div>
    );
  }
}

export default TopLeft1;

// const Cards = () => {
//   return (
//     <div className="container">
//       <div className="box1">
//           <div className="Card">

//           </div>
//       </div>
//       <div className="box"></div>
//       <div className="box"></div>
//       <div className="box"></div>
//       <div className="rec"></div>
//       <div className="box2"></div>
//       <div className="box2"></div>
//       <div className="box2"></div>
//       <div className="box3"></div>
//       <div className="box3"></div>
//       <div className="box3"></div>
//     </div>
//   );
// };

// export default Cards;
